#! /bin/bash
## __author__ is humingzhe
## Email:admin@humingzhe.com
echo 1 > /proc/sys/net/ipv4/ip_forward
ipv=/usr/sbin/ipvsadm
vip=192.168.222.200
rs1=192.168.222.128
rs2=192.168.222.130
#设置虚拟网卡，网卡名为 ens33:2，IP 为 192.168.222.200
ifdown ens33
ifup ens33
ifconfig ens33:2 $vip broadcast $vip netmask 255.255.255.255 up
#设置网关
route add -host $vip dev ens33:2
$ipv -C
$ipv -A -t $vip:80 -s rr
$ipv -a -t $vip:80 -r $rs1:80 -g -w 1
$ipv -a -t $vip:80 -r $rs2:80 -g -w 1